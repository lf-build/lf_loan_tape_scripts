var TransactionLog = require('./transaction-log.js')
var InvestorDailyFeed = require('./investor-daily-feed.js')
var envvars = require('./envvars.js');
var cron = require('cron');
var CronJob = cron.CronJob;

function uploadTransactionLog() {
  var feed = new TransactionLog();
  feed.generate().
    then(function(file) {
      file.upload().
        then(function() { return file.rm(); }).
        fail(function() { console.log('Error:', arguments) }).
        done();
    }).
    fail(function() { console.log('Error:', arguments) }).
    done();
}

function uploadInvestorDailyFeed() {
  var feed = new InvestorDailyFeed();
  feed.generate().
    then(function(file) {
      file.upload().
        then(function() { return file.rm(); }).
        fail(function() { console.log('Error:', arguments) }).
        done();
    }).
    fail(function() { console.log('Error:', arguments) }).
    done();
}

function schedule(jobName, job, cronExpression) {
  console.log('Scheduling job "%s" with cron expression %s', jobName, cronExpression);
  new CronJob(cronExpression, function() {
    console.log('Job triggered: %s', jobName);
    job();
  }, null, true, 'America/Detroit');
}

module.exports = {
  schedule: function() {
    schedule('Transaction Log', uploadTransactionLog, envvars.transactionlog.job.cronExpression);
    schedule('Investor Daily Feed', uploadInvestorDailyFeed, envvars.investor.job.cronExpression);
  }
};
