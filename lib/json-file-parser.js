var fs = require('fs');
var abort = require('./abort.js');

function parseJsonFile(file) {
  var config = JSON.parse(fs.readFileSync(file, 'utf8'));
  if (!config)
    abort('failed to parse json file: '+file);
  return config;
}

module.exports = parseJsonFile;
