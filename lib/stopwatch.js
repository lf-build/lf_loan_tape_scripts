function StopWatch() {}

StopWatch.prototype.start = function start() {
  this.startTime = new Date();
  return this;
}

StopWatch.prototype.stop = function stop() {
  this.stopTime = new Date();
  return this;
}

StopWatch.prototype.printDuration = function printDuration() {
  console.log('Total time: %d ms', this.stopTime.getTime() - this.startTime.getTime());
  return this;
}

module.exports = StopWatch;
