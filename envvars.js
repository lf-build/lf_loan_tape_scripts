function required(name) {
  var value = process.env[name];
  if (typeof value === 'undefined' || value === null || value.length === 0)
    throw 'Required environment variable not set: ' + name;
  return value;
}

function optional(name, defaultValue) {
  return process.env[name] || defaultValue;
}

module.exports = {
  common: {
    tenantId: required('TENANT_ID'),
    token: required('TOKEN'),
    mongoUrl: required('MONGO_URL'),
    services: {
      configuration: {
        host: optional('CONFIGURATION_HOST', 'configuration'),
        root: optional('CONFIGURATION_ROOT_PATH')
      }
    },
    box: {
      token: required('BOX_TOKEN')
    }
  },
  transactionlog: {
    configFile: required('TRANSACTION_LOG_CONFIG_FILE'),
    box: {
      folderId: required('TRANSACTION_LOG_BOX_FOLDER_ID'),
      fileId: required('TRANSACTION_LOG_BOX_FILE_ID')
    },
    job: {
      cronExpression: required('TRANSACTION_LOG_CRON_EXPRESSION')
    }
  },
  investor: {
    configFile: required('INVESTOR_DAILY_FEED_CONFIG_FILE'),
    box: {
      folderId: required('INVESTOR_DAILY_FEED_BOX_FOLDER_ID'),
      fileId: required('INVESTOR_DAILY_FEED_BOX_FILE_ID')
    },
    job: {
      cronExpression: required('INVESTOR_DAILY_FEED_CRON_EXPRESSION')
    }
  }
};
