var _ = require('lodash');
var Q = require('q');
var moment = require('moment-timezone');
var util = require('util');
var fs = require('fs');
var path = require('path');
var os = require('os');
var request = require('request');
var MongoClient = require('mongodb').MongoClient;
var interval = require('./lib/interval.js');
var parseJsonFile = require('./lib/json-file-parser.js');
var SecureHttp = require('./lib/secure-http.js');
var StopWatch = require('./lib/stopwatch.js');
var envvars = require('./envvars.js');
var endOfLine = os.EOL;

moment.tz.setDefault("America/Detroit");

var InvestorDailyFeed = function() {};

InvestorDailyFeed.prototype.generate = function(startDate, endDate) {
  var period = {
    start: startDate ? moment(startDate) : null,
    end  : endDate ? moment(endDate).add(23, 'hours').add(59, 'minutes').add(59, 'seconds').add(999, 'milliseconds') : moment()
  };

  if (period.start && !period.start.isValid()) return Q.reject('invalid start date');
  if (period.end && !period.end.isValid()) return Q.reject('invalid end date');

  var configuration = loadConfiguration();

  return new Script(period, configuration).run();
};

function Script(period, configuration) {
  this.run = function run() {
    console.log('Reading transactions from %s to %s',
      period.start ? period.start.format('MM/DD/YYYY') : '(no start date)',
      period.end ? period.end.format('MM/DD/YYYY') : '(no end date)');

    var db = new Database(configuration);
    var data = {};
    var stopWatch = new StopWatch();

    stopWatch.start();

    return db.getTransactions(period).
      then(function(transactions) {
        console.log('Found %d transactions', transactions.length);
        data.transactions = transactions;
        data.refNumbers = transactions.map(function(transaction) { return transaction.LoanReference; });
        return db.getLoans(data.refNumbers);
      }).
      then(function(loans) {
        data.loans = loans;
        return db.getBorrowers(data.refNumbers);
      }).
      then(function(borrowers) {
        data.borrowers = borrowers;
        return db.getSchedules(data.refNumbers);
      }).
      then(function(schedules) {
        data.schedules = schedules;
        return getInvestors();
      }).
      then(function(investors) {
        data.investors = investors;
        var transactions = denormalize(merge(data));
        stopWatch.stop().printDuration();
        return writeToFile(transactions);
      });
  }

  function getInvestors() {
    var http = new SecureHttp(configuration.token);
    var deferred = Q.defer();

    http.get(configuration.services.configuration, '/investors/investors').then(function(response) {
      var investors = {};
      response.body.forEach(function(investor) {
        investors[investor.id] = investor;
      });
      deferred.resolve(investors);
    });

    return deferred.promise;
  }

  function merge(data) {
    var transactions = [];

    data.transactions.forEach(function(transaction) {
      transaction.Loan = data.loans[transaction.LoanReference];

      // Do not include transactions when the loan is not found.
      // The reason a loan is not found is because it was filtered out by the
      // query that filters loans by investor. Check the loans query down below.
      if (!transaction.Loan)
        return;

      transaction.Borrower = data.borrowers[transaction.LoanReference];
      transaction.Investor = data.investors[transaction.Loan.Investor._id];
      transaction.Schedule = data.schedules[transaction.LoanReference];

      if (transaction.PaymentId) {
        transaction.Installment = transaction.Schedule.Installments.find(function(installment) {
          return installment.PaymentId === transaction.PaymentId;
        });
      }

      transactions.push(transaction);
    });

    console.log('Excluding %d transactions from other investors', data.transactions.length - transactions.length);

    return transactions;
  }

  function denormalize(transactions) {
    var InstallmentType = {
      Additional: { code: 1, transactionDescription: 'Extra Payment' }
    };

    function getScheduleDate(transaction) {
      if (transaction.Installment) {
        if (parseInt(transaction.Installment.PaymentMethod) === 2) { // If it's a check payment
          if(transaction.Installment.AnniversaryDate) { // If it's an scheduled installment
            return moment(transaction.Installment.DueDate.DateTime);
          }
          else { // If it's an extra, payoff, etc
            return null;
          }
        }
        else { // If it's ACH payment
          return moment(transaction.Installment.DueDate.DateTime);
        }
      }

      /* Hack: if it's P&I or P&I reversal and there's no installment associated,
         use the transaction date as schedule date */
      if (transaction.Code === '124' || transaction.Code === '125')
        return moment(transaction.Date.DateTime);

      return null;
    }

    function getTransactionDescription(transaction) {
      if (transaction.Installment && transaction.Installment.Type == InstallmentType.Additional.code)
        return InstallmentType.Additional.transactionDescription;

      return transaction.Description;
    }

    function getLoanAge(transaction) {
      var installment = transaction.Installment;
      var date = transaction.Date.DateTime;
      var origination = transaction.Loan.Terms.OriginationDate.DateTime;
      for(var period = 1; ; period++) {
        var anniversary = moment(origination).add(period, 'months');
        if(moment(date).isBefore(anniversary)) {
          return period - 1;
        }
      }
      throw 'getLoanAge(): should not reach this point';
    }

    function isReversal(code) {
      return configuration.transactions.codes.reversals.indexOf(parseInt(code)) > -1;
    }

    function isDebit(code) {
      return configuration.transactions.codes.debits.indexOf(parseInt(code)) > -1;
    }

    function getPaymentMethodName(code) {
      if (code === 1) return 'ACH';
      if (code === 2) return 'Check';
      return 'Unknown';
    }

    var flatList = [];
    transactions.forEach(function(t) {
      flatList.push({
        FeedDate: moment(t.Timestamp.DateTime),
        Entity: t.Investor.name,
        LoanNumber: t.LoanReference,
        OriginationDate: moment(t.Loan.Terms.OriginationDate.DateTime),
        BorrowerLast: t.Borrower.LastName.substr(0, 3).toUpperCase(),
        BorrowerFirst: t.Borrower.FirstName.substr(0, 1).toUpperCase(),
        TransactionNumber: isReversal(t.Code) ? '' : t._id,
        ReversalNumber: isReversal(t.Code) ? t._id : '',
        TransactionDate: moment(t.Date.DateTime),
        DueDate: getScheduleDate(t),
        LoanAge: getLoanAge(t),
        TransactionAmount: t.Amount,
        Description: getTransactionDescription(t),
        PaymentMethod: getPaymentMethodName(t.Loan.PaymentMethod),
        Principal: t.Installment ? t.Installment.Principal : 0,
        Interest: t.Installment ? t.Installment.Interest : 0,
        Fees: 0,
        LateCharges: 0,
        EffectivePaymentAmount: parseFloat(t.Amount) * (isDebit(t.Code) ? -1 : 1)
      });
    });
    return flatList;
  }

  function write(transactions, callback) {
    var fields = {
      'Feed Date'               : 'FeedDate',
      'Entity'                  : 'Entity',
      'Loan Number'             : 'LoanNumber',
      'Origination Date'        : 'OriginationDate',
      'Borrower Last'           : 'BorrowerLast',
      'Borrower First'          : 'BorrowerFirst',
      'Transaction No'          : 'TransactionNumber',
      'Reversal No'             : 'ReversalNumber',
      'Transaction Date'        : 'TransactionDate',
      'Sch Due Date'            : 'DueDate',
      'Sch Loan Age'            : 'LoanAge',
      'Transaction Amount'      : 'TransactionAmount',
      'Description'             : 'Description',
      'Payment Method'          : 'PaymentMethod',
      'Principal'               : 'Principal',
      'Interest'                : 'Interest',
      'Fees'                    : 'Fees',
      'Late Charges'            : 'LateCharges',
      'Effective Payment Amount': 'EffectivePaymentAmount'
    };

    function printDate(date) {
      if(date) return date.format('MM/DD/YYYY');
      return '';
    }

    function writeHeader() {
      var header = '';
      for(var fieldTitle in fields) {
        if (header.length) header += ',';
        header += fieldTitle;
      }
      callback(header);
    }

    function writeBody() {
      transactions.forEach(function(transaction) {
        var line = '';
        for(var fieldTitle in fields) {
          var fieldName = fields[fieldTitle];
          if (line.length) line += ',';
          line += getFieldValue(fieldName, transaction);
        }
        callback(line);
      });
    }

    function getFieldValue(fieldName, transaction) {
      var fieldValue = transaction[fieldName];

      if (moment.isMoment(fieldValue))
        fieldValue = printDate(fieldValue.utc());

      if (typeof fieldValue === 'undefined' || fieldValue === null)
        return '';

      return fieldValue;
    }

    writeHeader();
    writeBody();
  }

  function writeToFile(transactions) {
    function dirExists(filePath) {
      try { return fs.statSync(filePath).isDirectory(); }
      catch (err) { return false; }
    }

    var text = '';
    write(transactions, function(line) { text += line + endOfLine; });

    var dir = configuration.output || os.tmpdir();

    if (!dirExists(dir))
      fs.mkdirSync(dir);

    var filepath = path.join(dir, util.format('investor-daily-feed_%d.csv', new Date().getTime()));

    var deferred = Q.defer();

    fs.writeFile(filepath, text, function (err) {
      if(err) return deferred.reject(err);
      console.log('Report saved to %s', filepath);
      deferred.resolve(new FeedFile(filepath));
    });

    return deferred.promise;
  }

  var FeedFile = function(filepath) {
    this.filepath = filepath;
  };

  FeedFile.prototype.upload = function() {
    var filepath = this.filepath;
    var token = configuration.box.token;
    var folderId = configuration.box.folderId;
    var fileId = configuration.box.fileId;

    if (!token) return Q.reject('Box token not set');
    if (!folderId) return Q.reject('Box folderId not set');
    if (!fileId) return Q.reject('Box fileId not set');

    console.log('Uploading to Box file %s in folder %s', fileId, folderId);

    var deferred = Q.defer();

    var r = request.post({
        url: 'https://upload.box.com/api/2.0/files/'+fileId+'/content',
        headers: { 'Authorization': 'Bearer ' + token }
      }, function(err, response) {
        if (err) {
          return deferred.reject(err);
        }

        if (typeof response === 'undefined' || response === null)
          return deferred.reject('Got null response from Box');

        if (response.statusCode >= 200 && response.statusCode < 300) {
          console.log('File uploaded (http %s %s)', response.statusCode, response.statusMessage);
          deferred.resolve();
        }
        else {
          return deferred.reject('Failed to upload file to Box: http ' + response.statusCode + ' ' + response.statusMessage);
        }
      });

    var form = r.form();
    form.append('folder_id', folderId);
    form.append("filename", fs.createReadStream(filepath));

    return deferred.promise;
  };

  FeedFile.prototype.rm = function() {
    var filepath = this.filepath;
    console.log('Deleting local file %s', filepath);
    fs.unlinkSync(filepath);
  };
}

function Database(configuration) {
  function getUrl(db) {
    return util.format(configuration.mongoUrl, db);
  };

  this.getTransactions = function getTransactions(period) {
    var deferred = Q.defer();

    MongoClient.connect(getUrl('loan-transaction'), function(err, db) {
      if (err) return deferred.reject(err);

      var query = {
        TenantId: configuration.tenantId,
        'Code': {
          $ne: '142' // exclude origination fee
        }
      };

      if (period.start || period.end) {
        var dateQuery = {};
        if (period.start) dateQuery['$gte'] = period.start.toDate();
        if (period.end) dateQuery['$lte'] = period.end.toDate();
        query['Date.DateTime'] = dateQuery;
      }

      var projection = {
        'Timestamp.DateTime': true,
        'Date.DateTime': true,
        Code: true,
        Amount: true,
        LoanReference: true,
        Description: true,
        PaymentId: true
      };

      var sort = {
        'Date.DateTime': 1,
        LoanReference: 1
      };

      db.collection('transactions').find(query, projection).sort(sort).toArray(function(err, docs) {
        if (err) return deferred.reject(err);

        var transactions = [];
        docs.forEach(function(transaction) {
          transactions.push(transaction);
        });
        db.close();

        deferred.resolve(transactions);
      });
    });

    return deferred.promise;
  }

  this.getLoans = function getLoans(refNumbers) {
    var deferred = Q.defer();

    MongoClient.connect(getUrl('loans'), function(err, db) {
      if (err) return deferred.reject(err);

      var query = {
        TenantId: configuration.tenantId,
        ReferenceNumber: { $in: refNumbers },
        'Investor._id': { $in: configuration.filters.investors }
      };

      var projection = {
        ReferenceNumber: true,
        'Investor._id': true,
        PaymentMethod: true,
        'Terms.OriginationDate.DateTime': true
      };

      db.collection('loans').find(query, projection).toArray(function(err, docs) {
        if (err) return deferred.reject(err);

        var result = {};
        docs.forEach(function(loan) {
          result[loan.ReferenceNumber] = loan;
        });
        db.close();

        deferred.resolve(result);
      });
    });

    return deferred.promise;
  }

  this.getBorrowers = function getBorrowers(refNumbers) {
    var deferred = Q.defer();

    MongoClient.connect(getUrl('loans'), function(err, db) {
      if (err) return deferred.reject(err);

      var query = {
        TenantId: configuration.tenantId,
        LoanReferenceNumber: { $in: refNumbers }
      };

      var projection = {LoanReferenceNumber: true, FirstName: true, LastName: true};

      db.collection('borrowers').find(query, projection).toArray(function(err, docs) {
        if (err) return deferred.reject(err);

        var borrowers = {};
        docs.forEach(function(doc) {
          borrowers[doc.LoanReferenceNumber] = doc;
        });
        db.close();

        deferred.resolve(borrowers);
      });
    });

    return deferred.promise;
  }

  this.getSchedules = function getSchedules(refNumbers) {
    var deferred = Q.defer();

    MongoClient.connect(getUrl('loans'), function(err, db) {
      if (err) return deferred.reject(err);

      var query = {
        TenantId: configuration.tenantId,
        LoanReferenceNumber: { $in: refNumbers }
      };

      var projection = {
        LoanReferenceNumber: true,
        'Installments.PaymentId': true,
        'Installments.Principal': true,
        'Installments.Interest': true,
        'Installments.AnniversaryDate.DateTime': true,
        'Installments.DueDate': true,
        'Installments.Type': true,
        'Installments.PaymentMethod': true
      };

      db.collection('schedule').find(query, projection).toArray(function(err, docs) {
        if (err) return deferred.reject(err);

        var result = {};
        docs.forEach(function(doc) {
          result[doc.LoanReferenceNumber] = doc;
        });
        db.close();

        deferred.resolve(result);
      });
    });

    return deferred.promise;
  }
}

function loadConfiguration() {
  var filepath = envvars.investor.configFile;

  if (!filepath)
    throw 'Configuration file path is null or undefined';

  var configuration = {};
  var fromFile = parseJsonFile(filepath);
  _.defaultsDeep(configuration, envvars.common);
  _.defaultsDeep(configuration, envvars.investor);
  _.defaultsDeep(configuration, fromFile);

  function assertConfigIsPresent(name) {
    var entry = configuration[name];
    if (typeof entry === 'undefined' || entry === null)
      throw 'missing configuration: '+name;
  }

  assertConfigIsPresent('tenantId');
  assertConfigIsPresent('token');
  assertConfigIsPresent('mongoUrl');
  assertConfigIsPresent('services');
  assertConfigIsPresent('box');

  return configuration;
}

module.exports = InvestorDailyFeed;
