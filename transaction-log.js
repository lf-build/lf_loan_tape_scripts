var _ = require('lodash');
var Q = require('q');
var moment = require('moment-timezone');
var util = require('util');
var fs = require('fs');
var path = require('path');
var os = require('os');
var request = require('request');
var MongoClient = require('mongodb').MongoClient;
var interval = require('./lib/interval.js');
var parseJsonFile = require('./lib/json-file-parser.js');
var SecureHttp = require('./lib/secure-http.js');
var StopWatch = require('./lib/stopwatch.js');
var envvars = require('./envvars.js');
var endOfLine = os.EOL;

moment.tz.setDefault("America/Detroit");

var TransactionLog = function() {};

TransactionLog.prototype.generate = function(startDate, endDate) {
  var period = {
    start: startDate ? moment(startDate) : null,
    end  : endDate ? moment(endDate).add(23, 'hours').add(59, 'minutes').add(59, 'seconds').add(999, 'milliseconds') : moment()
  };

  if (period.start && !period.start.isValid()) return Q.reject('invalid start date');
  if (period.end && !period.end.isValid()) return Q.reject('invalid end date');

  var configuration = loadConfiguration();

  return new Script(period, configuration).run();
};

function Script(period, configuration) {
  this.run = function run() {
    console.log('Reading transactions from %s to %s',
      period.start ? period.start.format('MM/DD/YYYY') : '(no start date)',
      period.end ? period.end.format('MM/DD/YYYY') : '(no end date)');

    var db = new Database(configuration);
    var data = {};
    var stopWatch = new StopWatch();

    stopWatch.start();

    return db.getTransactions(period).
      then(function(transactions) {
        console.log('Found %d transactions', transactions.length);
        data.transactions = transactions;
        data.refNumbers = transactions.map(function(transaction) { return transaction.LoanReference; });
        return db.getLoans(data.refNumbers);
      }).
      then(function(loans) {
        data.loans = loans;
        return db.getSchedules(data.refNumbers);
      }).
      then(function(schedules) {
        data.schedules = schedules;
        var transactions = denormalize(merge(data));
        stopWatch.stop().printDuration();
        return writeToFile(transactions);
      });
  }

  function merge(data) {
    var transactions = [];

    data.transactions.forEach(function(transaction) {
      transaction.Loan = data.loans[transaction.LoanReference];
      transaction.Schedule = data.schedules[transaction.LoanReference];

      if (transaction.PaymentId) {
        transaction.Installment = transaction.Schedule.Installments.find(function(installment) {
          return installment.PaymentId === transaction.PaymentId;
        });
      }

      transactions.push(transaction);
    });
    return transactions;
  }

  function denormalize(transactions) {
    var InstallmentType = {
      Additional: { code: 1, transactionDescription: 'Extra Payment' }
    };

    function getTransactionDescription(transaction) {
      if (transaction.Installment && transaction.Installment.Type == InstallmentType.Additional.code)
        return InstallmentType.Additional.transactionDescription;

      return transaction.Description;
    }

    function getFees(fees) {
      var amount = 0;
      if (fees) {
        fees.forEach(function(fee){
          amount += fee.Amount;
        });
      }
      return Math.round(amount * 100.0) / 100.0;
    }

    var flatList = [];
    var rowNumber = 0;
    transactions.forEach(function(t) {
      var fees = getFees(t.Loan.Terms.Fees);

      flatList.push({
        TransactionNumber: ++rowNumber,
        PaymentDate: moment(t.Date.DateTime),
        LoanNumber: t.LoanReference,
        PaymentAmount: t.Amount,
        PaymentType: t.Installment ? (t.Installment.Type === 0 ? 'Scheduled' : 'Extra') : getTransactionDescription(t),
        BeginningBalance: t.Installment ? t.Installment.OpeningBalance : '',
        Principal: t.Installment ? t.Installment.Principal : '',
        Interest: t.Installment ? t.Installment.Interest : '',
        EndingBalance: t.Installment ? t.Installment.EndingBalance : '',
        Investor: t.Loan.Investor._id,
        OriginationDate: moment(t.Loan.Terms.OriginationDate.DateTime),
        FundedDate: t.Loan.Terms.FundedDate ? moment(t.Loan.Terms.FundedDate.DateTime) : '',
        OriginationFee: fees,
        LoanAmount: t.Loan.Terms.LoanAmount,
        FundedAmount: t.Loan.Terms.LoanAmount - fees,
        Rate: t.Loan.Terms.Rate,
        Term: t.Loan.Terms.Term,
        LateFeesPaid: '',
        OtherFeesPaid: '',
        TotalFeesPaid: '',
        ClosedDate: ''
      });
    });
    return flatList;
  }

  function write(transactions, callback) {
    var fields = {
      'Transaction No.': 'TransactionNumber',
      'Payment Date/Deposit Date': 'PaymentDate',
      'Loan Number': 'LoanNumber',
      'Payment Amount': 'PaymentAmount',
      'Payment Type': 'PaymentType',
      'Beginning Balance': 'BeginningBalance',
      'Principal': 'Principal',
      'Interest': 'Interest',
      'Ending Balance': 'EndingBalance',
      'Investor': 'Investor',
      'Origination Date': 'OriginationDate',
      'Funded Date': 'FundedDate',
      'Origination Fee$': 'OriginationFee',
      'Loan Amount': 'LoanAmount',
      'Funded Amount': 'FundedAmount',
      'Interest Rate': 'Rate',
      'Term': 'Term',
      'Late Fees Paid': 'LateFeesPaid',
      'Other Fees Paid': 'OtherFeesPaid',
      'Total Fees Paid': 'TotalFeesPaid',
      'Closed Date': 'ClosedDate'
    };

    function printDate(date) {
      if(date) return date.format('MM/DD/YYYY');
      return '';
    }

    function writeHeader() {
      var header = '';
      for(var fieldTitle in fields) {
        if (header.length) header += ',';
        header += fieldTitle;
      }
      callback(header);
    }

    function writeBody() {
      transactions.forEach(function(transaction) {
        var line = '';
        for(var fieldTitle in fields) {
          var fieldName = fields[fieldTitle];
          if (line.length) line += ',';
          line += getFieldValue(fieldName, transaction);
        }
        callback(line);
      });
    }

    function getFieldValue(fieldName, transaction) {
      var fieldValue = transaction[fieldName];

      if (moment.isMoment(fieldValue))
        fieldValue = printDate(fieldValue.utc());

      if (typeof fieldValue === 'undefined' || fieldValue === null)
        return '';

      return fieldValue;
    }

    writeHeader();
    writeBody();
  }

  function writeToFile(transactions) {
    function dirExists(filePath) {
      try { return fs.statSync(filePath).isDirectory(); }
      catch (err) { return false; }
    }

    var text = '';
    write(transactions, function(line) { text += line + endOfLine; });

    var dir = configuration.output || os.tmpdir();

    if (!dirExists(dir))
      fs.mkdirSync(dir);

    var filepath = path.join(dir, util.format('transaction-log_%d.csv', new Date().getTime()));

    var deferred = Q.defer();

    fs.writeFile(filepath, text, function (err) {
      if(err) return deferred.reject(err);
      console.log('Report saved to %s', filepath);
      deferred.resolve(new ReportFile(filepath));
    });

    return deferred.promise;
  }

  var ReportFile = function(filepath) {
    this.filepath = filepath;
  };

  ReportFile.prototype.upload = function() {
    var filepath = this.filepath;
    var token = configuration.box.token;
    var folderId = configuration.box.folderId;
    var fileId = configuration.box.fileId;

    if (!token) return Q.reject('Box token not set');
    if (!folderId) return Q.reject('Box folderId not set');
    if (!fileId) return Q.reject('Box fileId not set');

    console.log('Uploading to Box file %s in folder %s', fileId, folderId);

    var deferred = Q.defer();

    var r = request.post({
        url: 'https://upload.box.com/api/2.0/files/'+fileId+'/content',
        headers: { 'Authorization': 'Bearer ' + token }
      }, function(err, response) {
        if (err) {
          return deferred.reject(err);
        }

        if (typeof response === 'undefined' || response === null)
          return deferred.reject('Got null response from Box');

        if (response.statusCode >= 200 && response.statusCode < 300) {
          console.log('File uploaded (http %s %s)', response.statusCode, response.statusMessage);
          deferred.resolve();
        }
        else {
          return deferred.reject('Failed to upload file to Box: http ' + response.statusCode + ' ' + response.statusMessage);
        }
      });

    var form = r.form();
    form.append('folder_id', folderId);
    form.append("filename", fs.createReadStream(filepath));

    return deferred.promise;
  };

  ReportFile.prototype.rm = function() {
    var filepath = this.filepath;
    console.log('Deleting local file %s', filepath);
    fs.unlinkSync(filepath);
  };
}

function Database(configuration) {
  function getUrl(db) {
    return util.format(configuration.mongoUrl, db);
  };

  this.getTransactions = function getTransactions(period) {
    var deferred = Q.defer();

    MongoClient.connect(getUrl('loan-transaction'), function(err, db) {
      if (err) return deferred.reject(err);

      var query = {
        TenantId: configuration.tenantId,
        'Code': {
          $nin: ['100', '142']
        }
      };

      if (period.start || period.end) {
        var dateQuery = {};
        if (period.start) dateQuery['$gte'] = period.start.toDate();
        if (period.end) dateQuery['$lte'] = period.end.toDate();
        query['Date.DateTime'] = dateQuery;
      }

      var projection = {
        'Date.DateTime': true,
        Code: true,
        Amount: true,
        LoanReference: true,
        Description: true,
        PaymentId: true
      };

      var sort = {
        'Date.DateTime': 1,
        LoanReference: 1
      };

      db.collection('transactions').find(query, projection).sort(sort).toArray(function(err, docs) {
        if (err) return deferred.reject(err);

        var transactions = [];
        docs.forEach(function(transaction) {
          transactions.push(transaction);
        });
        db.close();

        deferred.resolve(transactions);
      });
    });

    return deferred.promise;
  }

  this.getLoans = function getLoans(refNumbers) {
    var deferred = Q.defer();

    MongoClient.connect(getUrl('loans'), function(err, db) {
      if (err) return deferred.reject(err);

      var query = {
        TenantId: configuration.tenantId,
        ReferenceNumber: { $in: refNumbers }
      };

      var projection = {
        ReferenceNumber: true,
        'Investor._id': true,
        PaymentMethod: true,
        'Terms.OriginationDate.DateTime': true,
        'Terms.FundedDate.DateTime': true,
        'Terms.LoanAmount': true,
        'Terms.Term': true,
        'Terms.Rate': true,
        'Terms.Fees': true
      };

      db.collection('loans').find(query, projection).toArray(function(err, docs) {
        if (err) return deferred.reject(err);

        var result = {};
        docs.forEach(function(loan) {
          result[loan.ReferenceNumber] = loan;
        });
        db.close();

        deferred.resolve(result);
      });
    });

    return deferred.promise;
  }

  this.getSchedules = function getSchedules(refNumbers) {
    var deferred = Q.defer();

    MongoClient.connect(getUrl('loans'), function(err, db) {
      if (err) return deferred.reject(err);

      var query = {
        TenantId: configuration.tenantId,
        LoanReferenceNumber: { $in: refNumbers }
      };

      var projection = {
        LoanReferenceNumber: true,
        'Installments.PaymentId': true,
        'Installments.OpeningBalance': true,
        'Installments.EndingBalance': true,
        'Installments.Principal': true,
        'Installments.Interest': true,
        'Installments.DueDate': true,
        'Installments.Type': true
      };

      db.collection('schedule').find(query, projection).toArray(function(err, docs) {
        if (err) return deferred.reject(err);

        var result = {};
        docs.forEach(function(doc) {
          result[doc.LoanReferenceNumber] = doc;
        });
        db.close();

        deferred.resolve(result);
      });
    });

    return deferred.promise;
  }
}

function loadConfiguration() {
  var filepath = envvars.transactionlog.configFile;

  if (!filepath)
    throw 'Configuration file path is null or undefined';

  var configuration = {};
  var fromFile = parseJsonFile(filepath);
  _.defaultsDeep(configuration, envvars.common);
  _.defaultsDeep(configuration, envvars.transactionlog);
  _.defaultsDeep(configuration, fromFile);

  function assertConfigIsPresent(name) {
    var entry = configuration[name];
    if (typeof entry === 'undefined' || entry === null)
      throw 'missing configuration: '+name;
  }

  assertConfigIsPresent('tenantId');
  assertConfigIsPresent('token');
  assertConfigIsPresent('mongoUrl');
  assertConfigIsPresent('box');

  return configuration;
}

module.exports = TransactionLog;
