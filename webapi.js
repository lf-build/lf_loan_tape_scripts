var TransactionLog = require('./transaction-log.js')
var InvestorDailyFeed = require('./investor-daily-feed.js');
var express = require('express');
var app = express();
var port= 5000;

function generateAndUpload(report, res) {
  function internalServerError(err) {
    console.log('Error:', err, arguments);
    res.status(500).send('Error: '+err);
  }

  report.generate().
    then(function(file) {
      file.upload().
        then(function() { return file.rm(); }).
        then(function() { res.send('OK'); }).
        fail(internalServerError).
        done();
    }).
    fail(internalServerError).
    done();
}

function run() {
  app.post('/transaction-log/upload', function (req, res) {
    console.log('Transaction log upload requested');
    generateAndUpload(new TransactionLog(), res);
  });

  app.post('/investor-daily-feed/upload', function (req, res) {
    console.log('Investor daily feed upload requested');
    generateAndUpload(new InvestorDailyFeed(), res);
  });

  app.listen(port, function () {
    console.log('Web server is listening on port %d', port);
  });
}

module.exports = { run: run };
