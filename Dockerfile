FROM node:4-onbuild

ADD . /transactionlogs
WORKDIR /transactionlogs
RUN npm install

EXPOSE 5000
ENTRYPOINT node server.js
